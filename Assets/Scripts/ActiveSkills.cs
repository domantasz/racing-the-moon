﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveSkills : MonoBehaviour
{

    public Transform Player;
    public Text Buff_Text1;
    public Text Buff_Text2;
    public Text Berserk_Text;
    public Image Berserk_Img;
    public Image Berserk_Cooldown;
    public Text Scream_Text;
    public Image Scream_Img;
    public Image Scream_Cooldown;
    public Text Ground_Text;
    public Image Ground_Img;
    public Image Ground_Cooldown;
    public Text ExtraLife_Text;
    float BerserkCooldown = 0;
    float BreakGroundCooldown = 0;
    float ScreamCooldown = 0;
    public bool BerserkActive = false;
    public bool SecondLifeActive = true;
    public bool BuffSkill = true;
    public bool BerserkSkill = true;
    public bool ScreamSkill = true;
    public bool SecondLifeSkill = true;
    public bool BreakGroundSkill = true;
    public AudioClip howl;
    public AudioClip buff;
    public AudioClip breakGround;
    private AudioSource source;
    int StoredBuff = 0;
    string[] buffList = { "None", "Double Points", "No damage", "Speed up", "Speed down", "Half points" };

    // Use this for initialization
    void Start()
    {
        GetSkills();
        BreakGroundCooldown = 0;
        ScreamCooldown = 0;
        BerserkEnd();
        SecondLifeActive = SecondLifeSkill;

        Buff_Text1.gameObject.SetActive(BuffSkill);
        Buff_Text2.gameObject.SetActive(BuffSkill);

        Berserk_Text.gameObject.SetActive(false);
        Berserk_Cooldown.gameObject.SetActive(false);
        Berserk_Img.gameObject.SetActive(BerserkSkill);

        Scream_Text.gameObject.SetActive(false);
        Scream_Cooldown.gameObject.SetActive(false);
        Scream_Img.gameObject.SetActive(ScreamSkill);

        Ground_Text.gameObject.SetActive(false);
        Ground_Cooldown.gameObject.SetActive(false);
        Ground_Img.gameObject.SetActive(BreakGroundSkill);


        ExtraLife_Text.gameObject.SetActive(SecondLifeSkill);

        source = Player.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        int time;
        if (BerserkCooldown != 0)
        {
            time = (int)BerserkCooldown;
            time += 1;
            Berserk_Text.text = time.ToString();
            BerserkCooldown -= Time.deltaTime;
            if (BerserkCooldown <= 0)
                BerserkEnd();
        }
        if (BreakGroundCooldown != 0)
        {
            time = (int)BreakGroundCooldown;
            time += 1;
            Ground_Text.text = time.ToString();
            BreakGroundCooldown -= Time.deltaTime;
            if (BreakGroundCooldown <= 0)
            {
                BreakGroundCooldown = 0;
                Ground_Text.gameObject.SetActive(false);
                Ground_Cooldown.gameObject.SetActive(false);
                Ground_Img.gameObject.SetActive(true);
            }
        }
        if (ScreamCooldown != 0)
        {
            time = (int)ScreamCooldown;
            time += 1;
            Scream_Text.text = time.ToString();
            ScreamCooldown -= Time.deltaTime;
            if (ScreamCooldown <= 0)
            {
                ScreamCooldown = 0;
                Scream_Cooldown.gameObject.SetActive(false);
                Scream_Text.gameObject.SetActive(false);
                Scream_Img.gameObject.SetActive(true);
            }
        }
        if (Input.GetKey(KeyCode.Keypad1))
            ClearBuffs();
        if (Input.GetKey(KeyCode.Keypad2))
            BerserkStart();
        if (Input.GetKey(KeyCode.Keypad3))
            BreakGround();
        if (Input.GetKey(KeyCode.Keypad4))
            UseBuff();
        if (Input.GetKey(KeyCode.Keypad5))
            Scream();
    }

    void ClearBuffs()
    {
        Player.GetComponent<PowerUps>().RemoveBuffs();

    }

    void BerserkStart()
    {
        if (!BerserkSkill)
            return;
        if (BerserkActive)
            return;
        source.PlayOneShot(howl);
        BerserkActive = true;
        BerserkCooldown = 9.99f;
        Player.GetComponent<OnCollision>().DMG_Enemy = 5;
        Berserk_Text.gameObject.SetActive(true);
        Berserk_Cooldown.gameObject.SetActive(true);
        Berserk_Img.gameObject.SetActive(false);
    }

    void BerserkEnd()
    {
        BerserkActive = false;
        Player.GetComponent<OnCollision>().DMG_Enemy = 20;
        BerserkCooldown = 0;
        Berserk_Text.gameObject.SetActive(false);
        Berserk_Cooldown.gameObject.SetActive(false);
        Berserk_Img.gameObject.SetActive(true);
        Berserk_Text.text = "9";
    }

    void BreakGround()
    {
        if (!BreakGroundSkill)
            return;
        if (Player.transform.position.y>2 || Player.transform.position.y < 1)
            return;
        if (BreakGroundCooldown == 0)
            BreakGroundCooldown = 30f;
        if (BreakGroundCooldown == 30)
        {
            source.PlayOneShot(breakGround);
            BreakGroundCooldown = 29.99f;
            Vector3 a = GetComponent<CreateGround>().ForceEvent();
            a.y -= 2;
            Player.position = a;
            Ground_Text.gameObject.SetActive(true);
            Ground_Cooldown.gameObject.SetActive(true);
            Ground_Img.gameObject.SetActive(false);
        }
    }

    public void SecondLife()
    {
        if (!SecondLifeSkill)
            return;
        source.PlayOneShot(buff);
        ExtraLife_Text.gameObject.SetActive(false);
        SecondLifeActive = false;
        Player.GetComponent<OnCollision>().Health = 100;
        GetComponent<CreateGround>().Restart();
        Player.position = new Vector3(0, 1, 0);
    }

    public void StoreBuff(int buffid)
    {
        StoredBuff = buffid;
        Buff_Text2.text = buffList[StoredBuff];
    }

    void UseBuff()
    {
        if (StoredBuff == 0)
            return;
        source.PlayOneShot(buff);
        //string a = "Using buff id " + StoredBuff;
        //print(a);
        Player.GetComponent<PowerUps>().randomPowerups(StoredBuff);
        StoredBuff = 0;
        Buff_Text2.text = buffList[StoredBuff];
    }

    void Scream()
    {
        if (!ScreamSkill)
            return;
        if (ScreamCooldown > 0)
            return;
        source.PlayOneShot(howl);
        int length = GetComponent<CreateGround>().objects.Count;
        for (int i = length - 1; i > 0; i--)
        {
            if (GetComponent<CreateGround>().objects[i].tag == "enemy")
            {
                Destroy(GetComponent<CreateGround>().objects[i]);
                GetComponent<CreateGround>().objects.RemoveAt(i);
            }
        }
        ScreamCooldown = 9.99f;
        Scream_Cooldown.gameObject.SetActive(true);
        Scream_Text.gameObject.SetActive(true);
        Scream_Img.gameObject.SetActive(false);
    }

    public void GetSkills()
    {
        StatController stats = Player.GetComponent<StatController>();
        BuffSkill = stats.IsBuffSkillUnlocked();
        BerserkSkill = stats.IsBerserkSkillUnlocked();
        ScreamSkill = stats.IsScreamSkillUnlocked();
        SecondLifeSkill = stats.IsSecondLifeSkillUnlocked();
        BreakGroundSkill = stats.IsBreakGroundSkillUnlocked();
    }
    public void Restart()
    {
        Start();
    }
}
