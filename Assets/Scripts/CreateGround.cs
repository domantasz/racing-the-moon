﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateGround : MonoBehaviour {
	public int SIENOS_ID = 3;
    public GameObject[] groundPresets;
    public GameObject[] obstaclePool;
    public GameObject[] enemyPool;
    public GameObject[] foodPool;
	public GameObject[] buffPool;
	public GameObject[] GrassModels;
    public GameObject[] RockModels;
    public GameObject Light;
    public GameObject Torch;
    private Transform playerTransform;
    private float spawnZ = 0.0f;
    private float spawnX = 0.0f;
    private float groundLength = 10.0f;
    private List<GameObject> tiles = new List<GameObject>();
    public List<GameObject> objects = new List<GameObject>();
    public List<GameObject> TunelioObjects = new List<GameObject>();
    private int diff = 1;
    private int spawn2next = 0;
    private int generationType = 1;
    private int timeTillNextEvent = 0;
    private float currX;
    private float roadX;
    private bool bridge = false;
    private float bridgeTile;
    private bool forced = false;
    private bool ReikiaGrass = false;
    private bool ReikiaAkmenu = false;
    private bool tuneliovidus = false;

    // Use this for initialization
    void Start () {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        for (int i = 0; i < 10; i++)
        {
            ReikiaGrass = true;
            Spawn();
            while (objects.Count != 0 && i < 2)
            {
                Destroy(objects[0]);
                objects.RemoveAt(0);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (playerTransform.position.z > spawnZ - 9*groundLength)
        {
            if (generationType == 1)
            {
                ReikiaGrass = true;
                Spawn();
            }
            else if (generationType == 2)
            {
                ReikiaGrass = false;
                Spawn2();
            }
            else if (generationType == 3)
            {
                ReikiaGrass = false;
                Spawn3();
            }
            else
            {
                ReikiaGrass = true;
                Spawn();
                ReikiaGrass = false;
                tuneliovidus = true;
                Spawn4();
                tuneliovidus = false;
            }
            if (TunelioObjects.Count != 0)
                DeleteTunelioObjects();
            DeleteTiles();
            DeleteObjects();
            RollForEvent();
        }
        if (generationType!=2)
        CheckX();
	}

    //Laukas
    private void Spawn()
    {
        currX = spawnX + 9*groundLength;
        for (int i=0; i<19; i++)
        {
            GameObject obj;
            obj = Instantiate(groundPresets[0]) as GameObject;
            obj.transform.SetParent(transform);
            Vector3 a = new Vector3(currX, 0, spawnZ);
            obj.transform.position = a;
            if (!(generationType == 4 && currX == roadX))
            objGenerate(currX - 3.33f, spawnZ + 3.33f, diff, 0);
            currX -= 10;
            tiles.Add(obj);
        }
        spawnZ += groundLength;
    }

    //Miestas
    private void Spawn2()
    {
        float y = 2.50f;
        GameObject obj;
        Vector3 a;
        float tempCurr = currX;
        currX -= 90;

        //vandens spawninimas
        for (int i=0; i<19; i++)
        {
            if (i != 9 && i != 9 + spawn2next)
            {
                obj = Instantiate(groundPresets[1]) as GameObject;
                obj.transform.SetParent(transform);
                a = new Vector3(currX, y-2.80f, spawnZ);
                obj.transform.position = a;
                tiles.Add(obj);
                if (timeTillNextEvent==10)
                {
                    obj = Instantiate(groundPresets[2]) as GameObject;
                    obj.transform.SetParent(transform);
                    a = new Vector3(currX-5, y - 3.50f, spawnZ-5);
                    obj.transform.position = a;
                    tiles.Add(obj);
                }

            }
            currX += 10;
        }
        currX = tempCurr;


        //namų spawninimas
            Vector3 scale = new Vector3(0.05f, 0.05f, 0.05f);
        int namoNr;
        int rnd;
        rnd = Random.Range(1, 3);
        if (rnd == 1)
            namoNr = 4;
        else
            namoNr = 12;
            obj = Instantiate(obstaclePool[namoNr]) as GameObject;
            obj.transform.SetParent(transform);
            a = new Vector3(currX - 8, 0, spawnZ);
            obj.transform.position = a;
            obj.transform.localScale = scale;
            obj.transform.Rotate(0, 90, 0);
            objects.Add(obj);
        obj = Instantiate(obstaclePool[16]) as GameObject;
        obj.transform.SetParent(transform);
        a = new Vector3(currX - 10, 0 - 0.1f, spawnZ - 5);
        obj.transform.position = a;
        objects.Add(obj);

        rnd = Random.Range(1, 3);
        if (rnd == 1)
            namoNr = 4;
        else
            namoNr = 12;

        obj = Instantiate(obstaclePool[namoNr]) as GameObject;
            obj.transform.SetParent(transform);
            a = new Vector3(currX + 18, 0, spawnZ);
            obj.transform.position = a;
            obj.transform.localScale = scale;
            obj.transform.Rotate(0, -90, 0);
            objects.Add(obj);
        obj = Instantiate(obstaclePool[16]) as GameObject;
        obj.transform.SetParent(transform);
        a = new Vector3(currX +20, 0-0.1f, spawnZ-5);
        obj.transform.position = a;
        objects.Add(obj);

        //lights
        obj = Instantiate(obstaclePool[11]) as GameObject;
        obj.transform.SetParent(transform);
        a = new Vector3(currX -4.5f, 0, spawnZ-1.5f);
        obj.transform.position = a;
        obj.transform.Rotate(0, 180, 0);
        objects.Add(obj);
        obj = Instantiate(Light) as GameObject;
        obj.transform.SetParent(transform);
        a = new Vector3(currX - 4.2f, 2.6f, spawnZ - 1.5f);
        obj.transform.position = a;
        objects.Add(obj);

        obj = Instantiate(obstaclePool[11]) as GameObject;
        obj.transform.SetParent(transform);
        a = new Vector3(currX - 4.5f, 0, spawnZ + 1.5f);
        obj.transform.position = a;
        obj.transform.Rotate(0, 180, 0);
        objects.Add(obj);
        obj = Instantiate(Light) as GameObject;
        obj.transform.SetParent(transform);
        a = new Vector3(currX - 4.2f, 2.6f, spawnZ + 1.5f);
        obj.transform.position = a;
        objects.Add(obj);

        obj = Instantiate(obstaclePool[11]) as GameObject;
        obj.transform.SetParent(transform);
        a = new Vector3(currX + 14.5f, 0, spawnZ-1.5f);
        obj.transform.position = a;
        objects.Add(obj);
        obj = Instantiate(Light) as GameObject;
        obj.transform.SetParent(transform);
        a = new Vector3(currX + 14.2f, 2.6f, spawnZ - 1.5f);
        obj.transform.position = a;
        objects.Add(obj);

        obj = Instantiate(obstaclePool[11]) as GameObject;
        obj.transform.SetParent(transform);
        a = new Vector3(currX + 14.5f, 0, spawnZ+1.5f);
        obj.transform.position = a;
        objects.Add(obj);
        obj = Instantiate(Light) as GameObject;
        obj.transform.SetParent(transform);
        a = new Vector3(currX + 14.2f, 2.6f, spawnZ + 1.5f);
        obj.transform.position = a;
        objects.Add(obj);

        //kelio spawninimas
        obj = Instantiate(groundPresets[0]) as GameObject;
        obj.transform.SetParent(transform);
        a = new Vector3(currX, 0, spawnZ);
        obj.transform.position = a;
        objGenerate(currX - 3.33f, spawnZ + 3.33f, diff, 0);
        tiles.Add(obj);

        obj = Instantiate(groundPresets[0]) as GameObject;
        obj.transform.SetParent(transform);
        a = new Vector3(currX+10f, 0, spawnZ);
        obj.transform.position = a;
        objGenerate(currX + 10f - 3.33f, spawnZ + 3.33f, diff, 0);
        tiles.Add(obj);

        if (timeTillNextEvent % 2 != 0)
        {
            spawn2next = Random.Range(-1, 0);
            if (spawn2next == 0)
                spawn2next = 1;
            currX += spawn2next * 10;
        }
        spawnZ += groundLength;
    }

    //Tiltas
    private void Spawn3()
    {
        currX = spawnX + 9 * groundLength;
        for (int i = 0; i < 19; i++)
        {
            GameObject obj;
            Vector3 a;
                obj = Instantiate(groundPresets[1]) as GameObject;
                a = new Vector3(currX, -0.3f, spawnZ);
                obj.transform.SetParent(transform);
                obj.transform.position = a;
                tiles.Add(obj);

                if (bridge)
                    obj = Instantiate(groundPresets[3]) as GameObject;
                else
                    obj = Instantiate(groundPresets[2]) as GameObject;
                a = new Vector3(currX-5, -1, spawnZ-5);
                obj.transform.SetParent(transform);
                obj.transform.position = a;
                tiles.Add(obj);
            if (currX == bridgeTile)
            {
                if (!bridge)
                {
                    Vector3 scale = new Vector3(70, 70, 70);

                    obj = Instantiate(groundPresets[4]) as GameObject;
                    a = new Vector3(currX+0.5f, -6.8f, spawnZ-23.6f);
                    obj.transform.localScale = scale;
                    obj.transform.SetParent(transform);
                    obj.transform.position = a;
                    obj.transform.Rotate(-10, 180, 0);
                    tiles.Add(obj);

                    obj = Instantiate(groundPresets[4]) as GameObject;
                    a = new Vector3(currX, 5.6f, spawnZ+26);
                    obj.transform.SetParent(transform);
                    obj.transform.localScale = scale;
                    obj.transform.position = a;
                    obj.transform.Rotate(-38, 0, 0);
                    tiles.Add(obj);

                    obj = Instantiate(groundPresets[4]) as GameObject;
                    a = new Vector3(currX, -6.7f, spawnZ+32.7f);
                    obj.transform.SetParent(transform);
                    obj.transform.localScale = scale;
                    obj.transform.position = a;
                    obj.transform.Rotate(-10, 0, 0);
                    tiles.Add(obj);
                }
            }
            currX -= 10;
        }
        spawnZ += groundLength;
        if (!bridge)
            bridge = true;
        else
        {
            generationType = 1;
            timeTillNextEvent = 4;
            bridge = false;
        }
    }

    //Tunelis
    private void Spawn4()
    {
        if (timeTillNextEvent == 10 || timeTillNextEvent == 1)
        {
            Vector3 pos = new Vector3(roadX, 0, spawnZ-10);
            Vector3 newpos;
            if (timeTillNextEvent == 10)
                newpos = new Vector3(roadX, -3.57f, spawnZ - 11.47f);
            else
                newpos = new Vector3(roadX, -3.57f, spawnZ - 10 + 1.47f);
            for (int i = 0; i < tiles.Count; i++)
            {
                if (tiles[tiles.Count-1-i].transform.position == pos)
                {
                    foreach (GameObject temp_obj in objects)
                    {
                        if (roadX - temp_obj.transform.position.x < 5 && roadX - temp_obj.transform.position.x > -5 && (spawnZ - 10) - temp_obj.transform.position.z < 5 && (spawnZ - 10) - temp_obj.transform.position.z > -5)
                            temp_obj.gameObject.SetActive(false);
                    }
                    if (timeTillNextEvent == 10)
                    {
                        tiles[tiles.Count - 1 - i].transform.Rotate(45, 0, 0);
                        TunelioObjects.Add(tiles[tiles.Count - 1 - i]);
                    }
                    else
                        tiles[tiles.Count - 1 - i].transform.Rotate(-45, 0, 0);
                    tiles[tiles.Count - 1 - i].transform.position = newpos;
                    break;
                }
            }
            if (timeTillNextEvent == 1)
            {
                spawn2next = 0;
            }
        }
        float y = -5 + 2.49f;
        GameObject obj;
        Vector3 a;
        Vector3 a2;
        int rot;
		obj = Instantiate(groundPresets[6]) as GameObject;
        obj.transform.SetParent(transform);
        if (spawn2next > 0)
        {
            a = new Vector3(roadX - 10, y, spawnZ - 10);
            a2 = new Vector3(roadX - 4.95f, y, spawnZ - 10);
            rot = -20;
        }
        else
        {
            a = new Vector3(roadX + 10, y, spawnZ - 10);
            a2 = new Vector3(roadX + 4.95f, y, spawnZ - 10);
            rot = 20;
        }
        obj.transform.position = a;
        objects.Add(obj);
        TunelioObjects.Add(obj);
        obj = Instantiate(Torch) as GameObject;
        obj.transform.SetParent(transform);
        obj.transform.position = a2;
        obj.transform.Rotate(0,0,rot);
        objects.Add(obj);
        TunelioObjects.Add(obj);
        obj = Instantiate(obstaclePool[16]) as GameObject;
        obj.transform.SetParent(transform);
        a.y = -6;
        a.z -= 5f;
        obj.transform.position = a;
        objects.Add(obj);
        TunelioObjects.Add(obj);

        obj = Instantiate(groundPresets[5]) as GameObject;
        obj.transform.SetParent(transform);
        a = new Vector3(roadX, -5, spawnZ-10);
        obj.transform.position = a;
        ReikiaAkmenu = true;
        if (timeTillNextEvent != 10 || timeTillNextEvent != 1)
        objGenerate(roadX - 3.33f, spawnZ-10 + 3.33f, diff, -5);
        ReikiaAkmenu = false;
        tiles.Add(obj);

        //lubos
        if (timeTillNextEvent != 10 && timeTillNextEvent != 1)
        {
            obj = Instantiate(groundPresets[5]) as GameObject;
            obj.transform.SetParent(transform);
            a = new Vector3(roadX, 0, spawnZ - 10);
            obj.transform.position = a;
            obj.transform.Rotate(180, 0, 0);
            tiles.Add(obj);
        }

        obj = Instantiate(groundPresets[6]) as GameObject;
        obj.transform.SetParent(transform);
        if (spawn2next != 0)
        {
            a = new Vector3(roadX + spawn2next * 20, y, spawnZ - 10);
            obj.transform.position = a;
            objects.Add(obj);
            TunelioObjects.Add(obj);
            obj = Instantiate(obstaclePool[16]) as GameObject;
            obj.transform.SetParent(transform);
            a.y -= 3.49f;
            a.z -= 5f;
            obj.transform.position = a;
            objects.Add(obj);
            TunelioObjects.Add(obj);
            a.y += 3.49f;
            a.z += 5f;

            obj = Instantiate(Torch) as GameObject;
            obj.transform.SetParent(transform);
            if (spawn2next > 0)
            {
                a.x -= 5.05f;
                rot = -20;
            }
            else
            {
                a.x += 5.05f;
                rot = 20;
            }
            obj.transform.position = a;
            obj.transform.Rotate(0, 0, rot);
            objects.Add(obj);
            TunelioObjects.Add(obj);


            roadX += spawn2next * 10;
            obj = Instantiate(groundPresets[5]) as GameObject;
            obj.transform.SetParent(transform);
            a = new Vector3(roadX, -5, spawnZ-10);
            obj.transform.position = a;
            ReikiaAkmenu = true;
            objGenerate(roadX - 3.33f, spawnZ-10 + 3.33f, diff, -5);
            ReikiaAkmenu = false;
            tiles.Add(obj);

            //lubos
            obj = Instantiate(groundPresets[5]) as GameObject;
            obj.transform.SetParent(transform);
            a = new Vector3(roadX, 0, spawnZ - 10);
            obj.transform.position = a;
            obj.transform.Rotate(180, 0, 0);
            tiles.Add(obj);

        }
        else
        {
            a = new Vector3(roadX - 10, y, spawnZ-10);
            obj.transform.position = a;
            objects.Add(obj);
            TunelioObjects.Add(obj);
            obj = Instantiate(obstaclePool[16]) as GameObject;
            obj.transform.SetParent(transform);
            a.y -= 3.49f;
            a.z -= 5f;
            obj.transform.position = a;
            a.y += 3.49f;
            a.z += 5f;
            objects.Add(obj);
            TunelioObjects.Add(obj);
            obj = Instantiate(Torch) as GameObject;
            obj.transform.SetParent(transform);
            a.x += 5.05f;
            obj.transform.position = a;
            obj.transform.Rotate(0, 0, -20);
            objects.Add(obj);
            TunelioObjects.Add(obj);
        }

        spawn2next = Random.Range(-1, 2);
        if (timeTillNextEvent == 1)
        {
            generationType = 1;
            spawn2next = 0;
            timeTillNextEvent += 5;
            forced = false;
        }
        //spawnZ += groundLength;
    }

    private void DeleteTiles()
    {
        while (tiles[0].transform.position.z - GameObject.FindGameObjectWithTag("Player").transform.position.z < -5)
        {
            Destroy(tiles[0]);
        tiles.RemoveAt(0);
        }
    }

    private void DeleteTunelioObjects()
    {
        while (TunelioObjects[0].transform.position.z - GameObject.FindGameObjectWithTag("Player").transform.position.z < -5)
        {
            TunelioObjects.RemoveAt(0);
            if (TunelioObjects.Count==0)
            {
                break;
            }
        }
    }

    private void DeleteObjects()
    {
        while (objects[0].transform.position.z - GameObject.FindGameObjectWithTag("Player").transform.position.z < -5)
        {
            Destroy(objects[0]);
            objects.RemoveAt(0);
        }
    }

    private void CheckX()
    {
        if (playerTransform.position.x > spawnX + groundLength/2)
        {
                spawnX += groundLength;
        }
        else if (playerTransform.position.x < spawnX - groundLength/2)
        {
            spawnX -= groundLength;
        }
    }

    private void objGenerate(float x, float z, int difficulty, float y)
    {
        int objProbability = 2 + 2 * difficulty;
        int itemRange = 6 + 2 * difficulty;
        //Tikrinimas ar bus kuriamas objektas. 1 - nebus, 2 ir 3 - bus.
        int a = Random.Range(1, objProbability);
        if (a > 1)
        {
            //Tikrinimas koks objektas bus kuriamas. 1 - obstacle, 2 - enemy, 3 - food, 4 - buff
            a = Random.Range(1, itemRange);
            if (forced)
            {
                a = Random.Range(itemRange-2, itemRange);
            }
            GameObject obj;
            if (a < (itemRange - 2) / 2)
            {
                // Kurioj vietoj bus dedamas objektas (kvadratą padalinau į 9 dalis)
                int h;
                if (generationType == 4 && tuneliovidus)
                {
                    h = Random.Range(13, 16);
                }
                else if (generationType == 2)
                {
                    h = Random.Range(5, 11);
                }
                else
                {
                    h = Random.Range(0, 3);
                }
				obj = Instantiate(obstaclePool[h]) as GameObject;
                /*obj.transform.SetParent(transform);
				Vector3 rot = new Vector3 (0,Random.Range(-45f,180f),0);
				obj.transform.Rotate (rot);*/
                if (generationType!=4)
                y = 0;
                if (h == 8)
                    y = 0.5f;
            }
            else if (a == itemRange-2)
            {
                obj = Instantiate(foodPool[0]) as GameObject;
                y = 0.75f;
            }
            else if (a == itemRange-1)
            {
                obj = Instantiate(buffPool[0]) as GameObject;
                y += 0.7f;
            }
            else
            {
                obj = Instantiate(enemyPool[0]) as GameObject;
                y -= 2f;
            }
            // Kurioj vietoj bus dedamas objektas (kvadratą padalinau į 9 dalis)
            a = Random.Range(0, 9);
            int b = a / 3;
            int c = a % 3;
            x = x + 3.33f * b;
            z = z - 3.33f * c;
            obj.transform.SetParent(transform);
            Vector3 d = new Vector3(x, y, z);
            obj.transform.position = d;
            //int rot = Random.Range(-45, 45);
            //obj.transform.Rotate(0, rot, 0);
            objects.Add(obj);
			//Generate grass
            if (ReikiaGrass)
			for (int i = 0; i < 8; i++) {	
				GameObject zole;
				int grassID = Random.Range (0, GrassModels.Length-1);
				zole = Instantiate (GrassModels[grassID]) as GameObject;
				zole.transform.SetParent (transform);
				Vector3 gPos = new Vector3 (Random.Range (x - 5f, x + 5f), 0, Random.Range (z - 5f, z + 5f));
				float s = Random.Range (0.90f, 1.20f);
				Vector3 gScale = new Vector3 (s,s,s);
				Vector3 gRot = new Vector3 (0,Random.Range (0,45f),0);
				//zole.transform.Rotate (gRot);
				zole.transform.localScale = gScale;
				zole.transform.position = gPos;
				objects.Add (zole);
			}
            // Akmenų generavimas
            if (ReikiaAkmenu)
                for (int i = 0; i < 8; i++)
                {
                    GameObject akmuo;
                    int stoneID = Random.Range(0, RockModels.Length - 1);
                    akmuo = Instantiate(RockModels[stoneID]) as GameObject;
                    akmuo.transform.SetParent(transform);
                    Vector3 gPos = new Vector3(Random.Range(x - 5f, x + 5f), -5f, Random.Range(z - 5f, z + 5f));
                    Vector3 gScale = new Vector3(2, 2, 2);
                    akmuo.transform.localScale = gScale;
                    akmuo.transform.position = gPos;
                    objects.Add(akmuo);
                }
        }
    }
		
    public void Restart()
    {
        while (TunelioObjects.Count != 0)
        {
            TunelioObjects.RemoveAt(0);
        }
        while (tiles.Count != 0)
        {
            Destroy(tiles[0]);
            tiles.RemoveAt(0);
        }
        while (objects.Count != 0)
        {
            Destroy(objects[0]);
            objects.RemoveAt(0);
        }
        generationType = 1;
        spawnZ = 0.0f;
        spawnX = 0.0f;
        Start();
    }

    public void SetDifficulty(int x)
    {
        diff = x;
    }

    private void RollForEvent()
    {
        if (timeTillNextEvent == 1 && generationType == 2)
        {
            generationType = 1;
            timeTillNextEvent = 4;
        }
        if (timeTillNextEvent != 0)
        {
            timeTillNextEvent--;
            return;
        }
        int a = Random.Range(1, 10);
        if (a == 1)
        {
            generationType = 1;
            timeTillNextEvent = 10;
        }
        else if (a == 9 && generationType != 2)
        {
            generationType = 2;
            timeTillNextEvent = 10;
            currX = spawnX + 5 * groundLength - groundLength * Random.Range(3, 8);
            spawn2next = 0;
        }
        else if (a == 2 && generationType == 1)
        {
            generationType = 3;
            timeTillNextEvent = 10;
            bridgeTile = spawnX + 5 * groundLength - groundLength * Random.Range(3, 8);
        }
        else if (a == 3 && generationType == 1)
        {
            generationType = 4;
            timeTillNextEvent = 10;
            roadX = spawnX + 9 * groundLength - groundLength * Random.Range(3, 8);
            spawn2next = 0;
        }
    }

    public Vector3 ForceEvent()
    {
        ReikiaGrass = true;
        Spawn();
        while(TunelioObjects.Count !=0)
        {
            TunelioObjects[0].SetActive(false);
            TunelioObjects.RemoveAt(0);
        }
        ReikiaGrass = false;
        forced = true;
        generationType = 4;
        timeTillNextEvent = 9;
        roadX = spawnX;
        spawn2next = 0;
        spawnZ -= 10 * groundLength;
        Vector3 pos = new Vector3(roadX, 0, spawnZ);
        int i = 0;
        while (true)
        {
            if (tiles[i].transform.position == pos)
            {
                Destroy(tiles[i]);
                tiles.RemoveAt(i);
                break;
            }
            i++;
        }
        spawnZ += groundLength;
        Spawn4();
        for (i = 0; i < 9; i++)
        {
            spawnZ += groundLength;
            Spawn4();
            timeTillNextEvent--;
        }
        generationType = 1;
        return pos;
    }

}
