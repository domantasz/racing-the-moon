﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Achievements
{
    [Serializable]
    public class Achievement
    {
        private SimpleAchievement achievement;
        private int[] points;
        private string[] titles;
        private string[] messages;
        private string[] conditions;
        private int conditionType;
        private int maxLevel;
        private int currentLevel;
        private int[] spriteIndex;

        public string PreviousTitle
        {
            get
            {
                if (Level > 0)
                    return titles[Level - 1];
                else return Title;
            }
        }

        public int Level
        {
            get { return currentLevel; }
        }
        public int MaxLevel
        {
            get { return maxLevel; }
        }
        public bool isUnlocked
        {
            get { return achievement.isUnlocked; }
        }
        public int Points
        {
            get { return achievement.Points; }
        }
        public int ConditionType
        {
            get { return conditionType; }
        }
        public string Title
        {
            get { return achievement.Title; }
        }
        public string Message
        {
            get { return achievement.Message; }
        }
        public string Condition
        {
            get { return achievement.Condition; }
        }
        public int SpriteIndex
        {
            get { return achievement.SpriteIndex; }
        }


        public Achievement(string[] titles, string[] messages, int[] points, int conditionType, string[] conditions, int[] spriteIndex)
        {
            if (messages != null && titles != null && points != null && conditions != null &&
                messages.Length > 0 && titles.Length > 0 && points.Length > 0 && conditions.Length > 0 && spriteIndex.Length > 0)
            {
                maxLevel = new int[] { titles.Length, messages.Length, points.Length, conditions.Length, spriteIndex.Length }.Min();
                if (titles.Length != messages.Length || messages.Length != points.Length || conditions.Length != messages.Length || messages.Length != spriteIndex.Length)
                {
                    
                    this.titles = new string[maxLevel];
                    this.messages = new string[maxLevel];
                    this.points = new int[maxLevel];
                    this.conditions = new string[maxLevel];
                    for (int i = 0; i < maxLevel; i++)
                    {
                        this.titles[i] = titles[i];
                        this.messages[i] = messages[i];
                        this.points[i] = points[i];
                        this.conditions[i] = conditions[i];
                        this.spriteIndex[i] = spriteIndex[i];
                    }
                }
                else
                {
                    this.titles = titles;
                    this.messages = messages;
                    this.points = points;
                    this.conditions = conditions;
                    this.spriteIndex = spriteIndex;
                }
                this.conditionType = conditionType;
                achievement = new SimpleAchievement(titles[0], messages[0], points[0], conditionType, conditions[0], spriteIndex[0]);
            }
        }

        public bool TryToUnlock(string condition)
        {

            if (achievement.TryToUnlock(condition))
            {
                currentLevel++;
                if (currentLevel < maxLevel) {
                    achievement = new SimpleAchievement(titles[currentLevel], messages[currentLevel], 
                        points[currentLevel], conditionType, conditions[currentLevel], spriteIndex[currentLevel]);
                }
                return true;
            }
            return false;
        }
    }
}
