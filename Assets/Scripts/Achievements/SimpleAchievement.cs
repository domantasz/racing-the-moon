﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Achievements
{
    [Serializable]
    public class SimpleAchievement
    {
        private bool unlocked;
        private int points;
        private int conditionType;
        private string title;
        private string message;
        private string condition;
        private int index;
        public bool isUnlocked
        {
            get { return unlocked; }
        }
        public int Points
        {
            get { return points; }
        }
        public int ConditionType
        {
            get { return conditionType; }
        }
        public string Title
        {
            get { return title; }
        }
        public string Message
        {
            get { return message; }
        }
        public string Condition
        {
            get { return condition; }
        }
        public int SpriteIndex
        {
            get { return index; }
        }

        public SimpleAchievement(string title, string message, int points, int conditionType, string condition, int spriteIndex)
        {
            this.index = spriteIndex;
            this.unlocked = false;
            this.points = points;
            this.conditionType = conditionType;
            this.title = title;
            this.message = message;
            this.condition = condition;
            
        }

        public bool TryToUnlock(string condition)
        {
            try {
                switch (conditionType)
                {
                    case 0:
                        if (int.Parse(condition) > int.Parse(this.condition))
                            return Unlock();
                        else return false;
                    case 1:
                        if (long.Parse(condition) > long.Parse(this.condition))
                            return Unlock();
                        else return false;
                    case 2:
                        if (bool.Parse(condition))
                            return Unlock();
                        else return false;
                    default: return false;
                }
            }
            catch(Exception e)
            {
                return false;
            }
        }
       
        private bool Unlock()
        {
            if (!unlocked)
            {
                unlocked = true;
                return true;
            }
            else return false;
        }
    }
}
