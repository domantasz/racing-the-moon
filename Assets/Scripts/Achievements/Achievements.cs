﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Achievements
{
    [Serializable]
    public class AchievementsContainer
    {
        public Achievement totalScoreOver;
        public Achievement singleRunScoreOver;
        public Achievement inGameTime;
        public Achievement deathCountOver;
        public Achievement damageTakenTotal;
        public Achievement damageTakenInSingleRun;
        public Achievement scoreCombo;
        public Achievement stayAliveInVeteran;
        public Achievement doNotTakeDamage;
        public SimpleAchievement checkHighscores;
        public Achievement safeModeCollected;
        public Achievement speedUpCollected;
        public Achievement speedDownCollected;
        public Achievement doubleScoreCollected;

        public AchievementsContainer()
        {
            totalScoreOver = achievementGenerator(0,
                new string[] { "9999", "42000", "100000", "500000", "1000000" },
                "Total score",
                new int[] {1, 2, 3, 4, 5 },
                "Points, points, points"
                );
            singleRunScoreOver = achievementGenerator(0,
                new string[] { "1000", "9000", "30000", "42000", "100000" },
                "Single run score",
                new int[] {6, 7, 8, 9, 10 },
                "True survivor"
                );
            deathCountOver = achievementGenerator(0,
                new string[] { "1", "5", "42", "69", "9000" },
                "Death count",
                new int[] {11, 12, 13, 14, 15 },
                "Even death can't stop me"
                );
            damageTakenTotal = achievementGenerator(0,
                new string[] { "100", "1000", "4200", "6900", "9000" },
                "Damage taken total", 
                new int[] {16, 17, 18, 19, 20 },
                 "You're really clumsy!"
                );
            damageTakenInSingleRun = achievementGenerator(0,
                new string[] { "100", "200", "300", "420", "690" },
                "Damage taken in a single run",
                new int[] {21, 22, 23, 24, 25 },
                "Too many trees in my way"
                );
            scoreCombo = achievementGenerator(0,
                new string[] { "2", "3", "4", "5", "10" },
                "Score combo",
                new int[] {26, 27, 28, 29, 30 },
                "Soooo much points"
                );
            safeModeCollected = achievementGenerator(0,
                new string[] { "5", "25", "42", "69", "9000" },
                "Safe mode buff collected",
                new int[] {31, 32, 33, 34, 35 },
                "You have to play safe"
                );
            speedDownCollected = achievementGenerator(0,
                new string[] { "5", "25", "42", "69", "9000" },
                "Speed down buff collected",
                new int[] {36, 37, 38, 39, 40 },
                "Its not even that slow"
                );
            speedUpCollected = achievementGenerator(0,
                new string[] { "5", "25", "42", "69", "9000" },
                "Speed up buff collected",
                new int[] {41, 42, 43, 44, 45 },
                 "Gotta go fast"
                );
            doubleScoreCollected = achievementGenerator(0,
                new string[] { "5", "25", "42", "69", "9000" },
                "Double score buff collected",
                new int[] {46, 47, 48, 49, 50 },
                 "I need more points"
                );
            checkHighscores = new SimpleAchievement("Who's the best?", "Check highscores", 50, 2, true.ToString(), 69);

            inGameTime = new Achievement(
                new string[] { "In Game Time is Over 5min", "In Game Time is Over 30min", "In Game Time is Over 1h", "In Game Time is Over 24h", "In Game Time is Over 7 days" },
                new string[] { "Play this game for 5min", "Play this game for 30min", "Play this game for 1h", "Play this game for 24h", "Play this game for 7 days" },
                new int[] { 10, 20, 30, 1000, 9000 }, 1,
                new string[] { "300000", "1800000", "3600000", "86400000", "604800000" },
                new int[] {51, 52, 53, 54, 55}
                );
            stayAliveInVeteran = new Achievement(
                new string[] { "Private", "Sergeant", "Lieutenant", "Captain", "Major", "Colonel", "General" },
                new string[] { "Survive in veteran for 1min", "Survive in veteran for 2min", "Survive in veteran for 3min", "Survive in veteran for 4min and 20 sec", "Survive in veteran for 5min", "Survive in veteran for 10min", "Survive in veteran for 1h" },
                new int[] { 10, 20, 50, 420, 690, 1000, 9000 }, 1,
                new string[] { "60000", "120000", "180000", "260000", "300000", "600000", "3600000" },
                new int[] {56, 57, 58, 59, 60, 61, 62}
                );
            doNotTakeDamage = new Achievement(
                new string[] { "Novice assassin", "Apprentice assassin", "Adept assassin", "Expert assassin", "Master assassin", "Legendary assassin" },
                new string[] { "Don't take damage for 1min", "Don't take damage for 2min", "Don't take damage for 3min", "Don't take damage for 4min and 20 sec", "Don't take damage for 5min", "Don't take damage for 1h" },
                new int[] { 10, 20, 69, 420, 1000, 9000 }, 1,
                new string[] { "60000", "120000", "180000", "260000", "300000", "3600000" },
                new int[] {63, 64, 65, 66, 67, 68}
                );
        }

        Achievement achievementGenerator(int type, string[] conditions, string keyWord, int[] index, string title)
        {
            int level = conditions.Length;
            string[] titles = new string[level];
            string[] messages = new string[level];
            int[] points = new int[level];
            for (int i = 0; i < level; i++)
            {
                titles[i] = title;
                messages[i] = String.Format("Reach {0} {1}", keyWord.ToLower(), conditions[i]);
                if (i <= 3)
                    points[i] = 10 + ((i ^ 2) * 10);
                else points[i] = (points.Sum() - 30) * 2;
            }
            return new Achievement(titles, messages, points, type, conditions, index);
        }

    }
}
