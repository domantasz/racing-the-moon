﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

class Score : MonoBehaviour
{
    public Text scoreText;
	public Text scoreTextX;
    public Text totalScoreText;
    public Text highscores;
    public const float defaultScoreMultiplier = 1.0f;
    private float difficultyScoreMultiplier = 1.0f;
    private float score = 0;
    private float scoreMultiplier = 1.0f;
    private StatController stat;
    private PlayerMotion playerMotion;

    void Start()
    {
        ResetScore();
        stat = GetComponent<StatController>();
        playerMotion = GetComponent<PlayerMotion>();
        setScore();
		scoreTextX.enabled = false;
    }
    void Update()
    {
        setScore();
    }

    public void SetDifficulty(int d)
    {
        switch (d)
        {
            case 1:
                difficultyScoreMultiplier = 0.5f;
                break;
            case 2:
                difficultyScoreMultiplier = 1.0f;
                break;
            case 3:
                difficultyScoreMultiplier = 2.0f;
                break;
            case 4:
                difficultyScoreMultiplier = 4.0f;
                break;
            default:
                break;
        }
    }

    void setScore()
    {
        score += playerMotion.getSpeed() * scoreMultiplier *difficultyScoreMultiplier* Time.deltaTime;
		scoreText.text = "Score: " + (int)score;
		if (scoreMultiplier > 1) {
			scoreTextX.text = "X" + scoreMultiplier.ToString ();
			scoreTextX.enabled = true;
		} else {
			scoreTextX.enabled = false;
		}
        int total = stat.GetTotalScore() + (int)score;
        totalScoreText.text = "Total Score: " + total;
    }

    public void AddToScoreMultiplier(float d)
    {
        if (d != 0)
            scoreMultiplier += d;
        else
            scoreMultiplier = defaultScoreMultiplier;
        stat.ScoreCombo((int)scoreMultiplier);
    }

    public void ResetScore()
    {
        scoreMultiplier = defaultScoreMultiplier;
        score = 0;
    }

    public void Death()
    {
        stat.Death((int)score);
    }

    public void WriteHighscores()
    {
        StringBuilder builder = new StringBuilder();
        foreach (int i in stat.GetHighscores())
        {
            builder.Append(i).Append(Environment.NewLine);
        }
        highscores.text = builder.ToString();
    }

    public void AddBonusScore(int a)
    {
        score += a;
    }

}

