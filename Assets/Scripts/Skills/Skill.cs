﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Skills
{
    public class Skill
    {
        public String title { get; private set; }
        public String description { get; private set; }
        public int price { get; private set; }
        public bool unlocked { get; private set; }
        public int spriteIndex { get; private set; }

        public Skill(String title, String description, int price, bool unlocked, int spriteIndex)
        {
            this.title = title;
            this.description = description;
            this.price = price;
            this.unlocked = unlocked;
            this.spriteIndex = spriteIndex;
        }
    }
}
