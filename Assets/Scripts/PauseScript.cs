﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;

public class PauseScript : MonoBehaviour
{

    public Transform Death;
    public Transform Paused;
    public Transform Player;
    public Transform StartingPoint;
    public Transform NewGameMenu;
    public Transform camera;
    public bool canPause = false; 

    void Start()
    {
        Time.timeScale = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape)) && canPause)
        {
            Pause(Paused);
        }
        if (Death.gameObject.activeInHierarchy == true)
        {
            canPause = false;
        }
    }
    void Pause(Transform menu)
    {
        if (Death.gameObject.activeInHierarchy == true)
        {
            canPause = false;
        }
        else
        {
            if (menu.gameObject.activeInHierarchy == false)
            {
                menu.gameObject.SetActive(true);
                Time.timeScale = 0;
            }
            else
            {
                menu.gameObject.SetActive(false);
                Time.timeScale = 1;
            }

        }        
    }
    public void pause()
    {
        Pause(Paused);
    }
    public void NewGame()
    {
        if (StartingPoint.GetComponent<ActiveSkills>().SecondLifeSkill)
        {
            StartingPoint.GetComponent<ActiveSkills>().SecondLifeActive = true;
            StartingPoint.GetComponent<ActiveSkills>().ExtraLife_Text.gameObject.SetActive(true);
        }
        StartingPoint.GetComponent<ActiveSkills>().Restart();
        NewGameMenu.gameObject.SetActive(false);
        Player.GetComponent<Score>().ResetScore();
        if (Paused.gameObject.activeInHierarchy == true)
            Paused.gameObject.SetActive(false);
        Player.position = new Vector3(0, 1, 0);
        StartingPoint.position = new Vector3(0, 1, 0);
        Death.gameObject.SetActive(false);
        Time.timeScale = 1;
        StartingPoint.GetComponent<CreateGround>().Restart();
        Player.GetComponent<OnCollision>().Health = 100;
        Player.GetComponent<Notifications>().ResetSeconds();
        canPause = true;
        Player.GetComponent<OnCollision>().UpdateUI();
        Player.GetComponent<PowerUps>().Allfalse();
        Player.GetComponent<PowerUps>().RemoveBuffs();
        camera.GetComponent<CameraMotor>().ResetCamera();
    }

    public void SetDifficulty(int d)
    {
        Player.GetComponent<PlayerMotion>().SetDifficulty(d);
        Player.GetComponent<Score>().SetDifficulty(d);
        Player.GetComponent<StatController>().StartGame(d);
        StartingPoint.GetComponent<CreateGround>().SetDifficulty(d);
        
        NewGame();
    }
    public void ChangeCanPause()
    {
        if (canPause)
            canPause = false;
        else
            canPause = true;
    }
}
