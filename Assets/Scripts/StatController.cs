﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;
using Assets.Scripts.Achievements;
using Assets.Scripts.Skills;
using UnityEngine.UI;

public class StatController : MonoBehaviour {

    Data data;
    float inGameTime;
    float runTime;
    float timeWihouthDmg;
    int damage;
    bool playing;
    int difficulty;
	// Use this for initialization
	void Start () {
        data = LoadSaveManager.LoadData();
        inGameTime = 0.0f;
        damage = 0;
        playing = false;
        difficulty = 0;
        runTime = 0.0f;
        timeWihouthDmg = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
        inGameTime += Time.deltaTime;
        if(inGameTime > 10)
        {
            Notify(data.AddInGameTime(10000));
            inGameTime -= 10;
        }
        if (playing)
        {
            runTime += Time.deltaTime;
            timeWihouthDmg += Time.deltaTime;
        }
	}

    void Notify(Achievement a)
    {
        if (a != null)
            GetComponent<Notifications>().AddMessage("Unlocked: " + a.PreviousTitle);
    }

    void Notify(SimpleAchievement a)
    {
        if (a != null)
            GetComponent<Notifications>().AddMessage("Unlocked: " + a.Title);
    }

    public void AddSpeedDown() { Notify(data.addSpeedDown()); }

    public void AddSpeedUp() { Notify(data.addSpeedUp()); }

    public void AddSafeMode() { Notify(data.addSafe()); }

    public void AddDoubleScore() { Notify(data.addDoubleScore()); }

    public void AddDmg(int dmg)
    {
        damage += dmg;
        Notify(data.AddDamage(dmg));
        Notify(data.DidNotTakeDmg(((long)timeWihouthDmg) * 1000));
        timeWihouthDmg = 0.0f;
    }

    public void ScoreCombo(int combo) { Notify(data.ScoreCombo(combo)); }

    public void Death(int score)
    {
        playing = false;
        if (difficulty == 4)
            Notify(data.StayAliveInVeteranTime((int)runTime));
        runTime = 0.0f;
        Notify(data.DidNotTakeDmg(((long)timeWihouthDmg) * 1000));
        timeWihouthDmg = 0.0f;
        Notify(data.AddDeath());
        Notify(data.maxDamage(damage));
        damage = 0;
        Notify(data.AddScore(score));
        Notify(data.highScore());
        LoadSaveManager.SaveGame(data);
    }

    public int GetTotalScore() { return data.GetTotalScore(); }

    public int[] GetHighscores()
    {
        Notify(data.highScore());
        Notify(data.CheckHighscores());
        LoadSaveManager.SaveGame(data);
        return data.GetHighscores();
    }

    public void StartGame(int dif)
    {
        difficulty = dif;
        playing = true;
    }

    public List<Achievement> GetAchievements()
    {
        return data.GetAchievements();
    }

    public List<SimpleAchievement> GetSimpleAchievements()
    {
        return data.GetSimpleAchievements();
    }

    public void BuySkill(Transform skillTitle)
    {
        string title = skillTitle.GetComponent<Text>().text;

        if (title.Equals("Buff"))
            BuyBuffSkill();
        if (title.Equals("Berserk"))
            BuyBerserkSkill();
        if (title.Equals("Howl"))
            BuyScreamSkill();
        if (title.Equals("Break Ground"))
            BuyBreakGroundSkill();
        if (title.Equals("Second Life"))
            BuySecondLifeSkill();
        LoadSaveManager.SaveGame(data);
    }

    public bool BuyBuffSkill()
    {
        return data.BuyBuffSkill();
    }

    public bool BuyBerserkSkill()
    {
        return data.BuyBerserkSkill();
    }

    public bool BuyScreamSkill()
    {
        return data.BuyScreamSkill();
    }

    public bool BuyBreakGroundSkill()
    {
        return data.BuyBreakGroundSkill();
    }

    public bool BuySecondLifeSkill()
    {
        return data.BuySecondLifeSkill();
    }

    public bool IsBuffSkillUnlocked()
    {
        return data.BuffSkillUnlocked;
    }

    public bool IsBerserkSkillUnlocked()
    {
        return data.BerserkSkillUnlocked;
    }

    public bool IsScreamSkillUnlocked()
    {
        return data.ScreamSkillUnlocked;
    }

    public bool IsBreakGroundSkillUnlocked()
    {
        return data.BreakGroundSkillUnlocked;
    }

    public bool IsSecondLifeSkillUnlocked()
    {
        return data.SecondLifeSkillUnlocked;
    }

    public List<Skill> GetSkills()
    {
        return data.GetSkills();
    }
}
