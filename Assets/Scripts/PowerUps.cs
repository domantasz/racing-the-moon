﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUps : MonoBehaviour
{
    //buffs
    public bool doublePoints;
    public bool safeMode1;
	public bool speedUp;
	//Anims
	public Image speedUpAnim;
	public Image speedDownAnim;
	public Image NoDamageAnim;

    //debuffs
    public bool speedDown;
    public bool halfpoints;

    private float powerupLength;
    private bool powerupActive;
    private bool[] activebuffs = new bool[6];
    private float[] buffstimer = new float[6];
    public float powerupLengthCounter;
    private int PointsUpCount;
    private int PointsDownCount;

    public Transform StartingPoint;
    public Transform Player;
    public AudioSource source;
    public AudioClip buff;
    public GameObject buffp;
    // Use this for initialization
    void Start()
    {
        buffstimer[0] = 0;
        buffstimer[1] = 0;
        buffstimer[2] = 0;
        buffstimer[3] = 0;
        buffstimer[4] = 0;
        activebuffs[0] = false;
        activebuffs[1] = false;
        activebuffs[2] = false;
        activebuffs[3] = false;
        activebuffs[4] = false;
        PointsUpCount = 0;
        PointsDownCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Timers(Time.deltaTime);
        if (powerupActive)
        {
            if (doublePoints)
            {
                Player.GetComponent<Score>().AddToScoreMultiplier(1);
                doublePoints = false;
                activebuffs[0] = true;
                buffstimer[0] = 10;
                PointsUpCount++;
				Player.GetComponent<Notifications> ().AddMessage ("Double Points activated");
            }
            if (halfpoints)
            {
                Player.GetComponent<Score>().AddToScoreMultiplier(-0.5f);
                halfpoints = false;
                activebuffs[4] = true;
                PointsDownCount++;
                buffstimer[4] = 10;
				Player.GetComponent<Notifications> ().AddMessage ("Half Points activated");
            }
            if (safeMode1)
            {
                Player.GetComponent<OnCollision>().safeMode = true;
                safeMode1 = false;
                activebuffs[1] = true;
                buffstimer[1] = 10;
				Player.GetComponent<Notifications> ().AddMessage ("No damage activated");
				NoDamageAnim.enabled = true;
            }
            if (speedUp)
            {
                Player.GetComponent<PlayerMotion>().ChangeSpeedMultiplier(2f);
                speedUp = false;
                activebuffs[2] = true;
                buffstimer[2] = 10;
                buffstimer[3] = 0;
                Player.GetComponent<Notifications> ().AddMessage ("Speed up activated");
				speedUpAnim.enabled = true;
            }
            if (speedDown)
            {
                Player.GetComponent<PlayerMotion>().ChangeSpeedMultiplier(0.5f);
                speedDown = false;
                activebuffs[3] = true;
                buffstimer[3] = 10;
                buffstimer[2] = 0;
                Player.GetComponent<Notifications> ().AddMessage ("Speed Down activated");
				speedDownAnim.enabled = true;
            }
            TurnOffExpiredBuffs();
        }
    }
    void OnTriggerEnter(Collider Other)
    {
        if (Other.gameObject.tag == "Buff")
        {
            source.PlayOneShot(buff);
            buffp.GetComponent<ParticleSystem>().Play();
            Allfalse();
            randomPowerups(0);
            Other.gameObject.SetActive(false);
        }
    }
    public void ActivatePowerUP()
    {
        powerupActive = true;
    }

    public void randomPowerups(int forcebuff)
    {
        int witch;
        if (forcebuff != 0)
            witch = forcebuff;
        else
        {
            witch = Random.Range(1, 6);
            if (StartingPoint.GetComponent<ActiveSkills>().BuffSkill)
            {
                StartingPoint.GetComponent<ActiveSkills>().StoreBuff(witch);
				MessageBuff (witch);
                return;
            }
        }
        switch (witch)
        {
            case 1:
                doublePoints = true;
                Player.GetComponent<StatController>().AddDoubleScore();
                break;
            case 2:
                safeMode1 = true;
                Player.GetComponent<StatController>().AddSafeMode();
                break;
            case 3:
                speedUp = true;
                Player.GetComponent<StatController>().AddSpeedUp();
                break;
            case 4:
				
                speedDown = true;
                Player.GetComponent<StatController>().AddSpeedDown();
                break;
			case 5:
                halfpoints = true;
                break;
            default:
                break;
        }
        ActivatePowerUP();
		//Debug.Log ("Powerup: "+ text);
    }
    public void Allfalse()
    {
        powerupActive = false;
        //buffs
        doublePoints = false;
        safeMode1 = false;
        speedUp = false;

        //debuffs
        halfpoints = false;
        speedDown = false;
    }
    public void RemoveBuffs()
    {
        Allfalse();
        for (int i = 0; i < 6; i++)
            buffstimer[i] = 0;
        Player.GetComponent<PlayerMotion>().ChangeSpeedMultiplier(0);
        Player.GetComponent<OnCollision>().safeMode = false;
        Player.GetComponent<Score>().AddToScoreMultiplier(0f);
        PointsUpCount = 0;
        PointsDownCount = 0;
		speedUpAnim.enabled = false;
		speedDownAnim.enabled = false;
		NoDamageAnim.enabled = false;
		//speedUpAnim.gameObject.SetActive(false);
        //Player.GetComponent<Notifications> ().AddMessage ("Buffs Removed");
    }

    private void Timers(float time)
    {
        buffstimer[0] -= time;
        buffstimer[1] -= time;
        buffstimer[2] -= time;
        buffstimer[3] -= time;
        buffstimer[4] -= time;
    }
    private void TurnOffExpiredBuffs()
    {
        if (buffstimer[0] <= 0 && activebuffs[0])
        {
            Player.GetComponent<Score>().AddToScoreMultiplier(-1f * PointsUpCount);
            activebuffs[0] = false;
            PointsUpCount = 0;
        }

        if (buffstimer[1] <= 0 && activebuffs[1])
        {
            Player.GetComponent<OnCollision>().safeMode = false;
            activebuffs[1] = false;
			NoDamageAnim.enabled = false;
        }

        if (buffstimer[2] <= 0 && activebuffs[2])
        {
            Player.GetComponent<PlayerMotion>().ChangeSpeedMultiplier(0);
            if (activebuffs[3])
                Player.GetComponent<PlayerMotion>().ChangeSpeedMultiplier(0.5f);
            activebuffs[2] = false;
			speedUpAnim.enabled = false;
        }

        if (buffstimer[3] <= 0 && activebuffs[3])
        {
            Player.GetComponent<PlayerMotion>().ChangeSpeedMultiplier(0);
            if (activebuffs[2])
                Player.GetComponent<PlayerMotion>().ChangeSpeedMultiplier(2f);
            activebuffs[3] = false;
			speedDownAnim.enabled = false;
        }

        if (buffstimer[4] <= 0 && activebuffs[4])
        {
            Player.GetComponent<Score>().AddToScoreMultiplier(0.5f * PointsDownCount);
            PointsDownCount = 0;
            activebuffs[4] = false;
        }


    }
	private void MessageBuff(int i)
	{
		switch (i) {
		case 1:
			Player.GetComponent<Notifications> ().AddMessage ("Double Points");
			break;
		case 2:
			Player.GetComponent<Notifications> ().AddMessage ("No damage");
			break;
		case 3:
			Player.GetComponent<Notifications> ().AddMessage ("Speed up");
			break;
		case 4:
			Player.GetComponent<Notifications> ().AddMessage ("Speed down");
			break;
		case 5:
			Player.GetComponent<Notifications> ().AddMessage ("Half points");
			break;
		default:
			break;
		}
	}
}
