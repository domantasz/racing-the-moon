﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour {
    public Transform MainMenu;
    public Transform OptionsMenu;
    public Transform NewGameMenu;
    public Transform CreditsMenu;
    public Transform SkillsMenu;
    public Transform HighscoresMenu;
    public Transform PauseMenu;
    public Transform DeathMenu;
    public Transform Player;
    public Transform Achievments;
    public Transform Background;
    public Transform UI;
    public Transform SkillsUI;
    public Text Totalscore;
    private StatController stat;
    // Use this for initialization
    void Start () {
        BackgroundRez();
        UI.gameObject.SetActive(false);
        SkillsUI.gameObject.SetActive(false);
        stat = Player.GetComponent<StatController>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void ChangeMainMenu()
    {
        if (MainMenu.gameObject.activeInHierarchy == true)
            MainMenu.gameObject.SetActive(false);
        else MainMenu.gameObject.SetActive(true);

    }
    public void ChangeOptionsMenu()
    {
        if (OptionsMenu.gameObject.activeInHierarchy == true)
            OptionsMenu.gameObject.SetActive(false);
        else OptionsMenu.gameObject.SetActive(true);

    }
    public void ChangeNewGameMenu()
    {
        if (NewGameMenu.gameObject.activeInHierarchy == true)
            NewGameMenu.gameObject.SetActive(false);
        else NewGameMenu.gameObject.SetActive(true);

    }
    public void ChangeCreditsMenu()
    {
        if (CreditsMenu.gameObject.activeInHierarchy == true)
            CreditsMenu.gameObject.SetActive(false);
        else CreditsMenu.gameObject.SetActive(true);

    }
    public void ChangeSkillsMenu()
    {
        if (SkillsMenu.gameObject.activeInHierarchy == true)
            SkillsMenu.gameObject.SetActive(false);
        else
        {
            SkillsMenu.gameObject.SetActive(true);
            RefreshTotalScore();
        }

    }

    public void RefreshTotalScore()
    {
        Totalscore.text = "Total Score: " + stat.GetTotalScore();
    }

    public void ChangeHighscoresMenu()
    {
        if (HighscoresMenu.gameObject.activeInHierarchy == true)
            HighscoresMenu.gameObject.SetActive(false);
        else
        {
            HighscoresMenu.gameObject.SetActive(true);
            Player.GetComponent<Score>().WriteHighscores();
        }

    }
    public void ChangePauseMenu()
    {
        if (PauseMenu.gameObject.activeInHierarchy == true)
            PauseMenu.gameObject.SetActive(false);
        else PauseMenu.gameObject.SetActive(true);

    }
    public void ChangeDeathMenu()
    {
        if (DeathMenu.gameObject.activeInHierarchy == true)
            DeathMenu.gameObject.SetActive(false);
        else DeathMenu.gameObject.SetActive(true);

    }
    public void ChangeAchievmentsMenu()
    {
        if (Achievments.gameObject.activeInHierarchy == true)
            Achievments.gameObject.SetActive(false);
        else
        {
            Achievments.gameObject.SetActive(true);
        }

    }
    public void Quit()
    {
        Application.Quit();
    }
    public void ChangeBackground(bool offon)
    {
        if(offon)
           Background.gameObject.SetActive(true);
       else Background.gameObject.SetActive(false);
       
    }
    public void BackgroundRez()
    {
        // Background.rectTransform.sizeDelta = new Vector2(width, height);
        //if(Screen.width>1920 || Screen.height>1080)
        Background.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
    }
    public void ChangeUI(bool on)
    {
        if(on)
        {
            UI.gameObject.SetActive(true);
            SkillsUI.gameObject.SetActive(true);
        }
        else
        {
            UI.gameObject.SetActive(false);
            SkillsUI.gameObject.SetActive(false);
        }
    }
}
