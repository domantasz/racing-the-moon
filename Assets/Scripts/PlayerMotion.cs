﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMotion : MonoBehaviour {

    private const float constGravity = 30.0f;
    private const float constSpeed = 10.0f;

	public Animation wolf_anim;
    public AudioSource source;

    private float gravity = 25.0f;//is set SetDifficulty()
    private float speedMult = 1.0f;
    private float difficulty = 1.0f;//is set SetDifficulty()
    private CharacterController controller;
    private OnCollision onColision;
    private float acc = 3.0f; //forward
    private float accS = 10.0f; //forward
    private float speedF = 10.0f; //forward
    private float speedS = 10.0f; //sides
    private Vector3 moveVector;
    private float verticalVelocity = 0.0f;
    public Transform camera;

    // Use this for initialization
    void Start() {
		wolf_anim.Play("02_walk");
        onColision = GetComponent<OnCollision>();
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        moveVector = Vector3.zero;
        Gravity();
        Move();
        Death();
        controller.Move(moveVector * Time.deltaTime);
    }


    void Death()
    {
        if (verticalVelocity < gravity * -3)
        {
            verticalVelocity = 0.0f;
            onColision.Death();
        }
    }

    void Gravity()
    {
        if (controller.isGrounded)
            verticalVelocity = -0.01f;
        else
            verticalVelocity -= gravity * Time.deltaTime;
    }
    void Move()
    {
        if (speedF < constSpeed * difficulty * speedMult)
            speedF += acc * Time.deltaTime;
        else
            speedF -= acc * Time.deltaTime;

        float h = Input.GetAxisRaw("Horizontal");
        if (h != 0 && speedS < speedF && speedS > speedF * -1)
        {
            if (speedS < constSpeed * difficulty && speedS > constSpeed * -1 * difficulty)
                speedS += accS * h * Time.deltaTime * 2 * difficulty;
        }
        else
        {
            if (speedS > 0)
                speedS -= accS * Time.deltaTime;
            else if (speedS < 0)
                speedS += accS * Time.deltaTime;
        }

        float c = 2f * difficulty;

        if (h < 0)
        {
            Quaternion target = Quaternion.Euler(0.0f, -30.0f, 0.0f);
            if (transform.rotation.y < -30)
                transform.rotation = Quaternion.Slerp(transform.rotation, target, -Time.deltaTime * c);
            else transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * c);
        }
        else if (h > 0)
        {
            Quaternion target = Quaternion.Euler(0.0f, 30.0f, 0.0f);
            if (transform.rotation.y > 30)
                transform.rotation = Quaternion.Slerp(transform.rotation, target, -Time.deltaTime * c);
            else transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * c);
        }
        else
        {
            Quaternion target = Quaternion.Euler(0.0f, 0.0f, 0.0f);
            if (transform.rotation.y != 0)
                transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * c);
        }


        //X (left and right)
        moveVector.x = speedS;
        //Y (up and down)
        moveVector.y = verticalVelocity;
        //Z (forward and backward
        moveVector.z = speedF;
        //PLAYER ROTATION


        //wolf animations
        if (AnimSpeed() < 0.5f)
        {
            if (wolf_anim.IsPlaying("01_Run"))
            {
                //wolf_anim.Stop ("01_Run");
                wolf_anim.Play("02_walk");
            }
        }
        else
        {
            if (wolf_anim.IsPlaying("02_walk") || wolf_anim.IsPlaying("03_creep"))
            {
                //wolf_anim.Stop ("02_walk");
                wolf_anim.Play("01_Run");
            }
            wolf_anim["01_Run"].speed = AnimSpeed();
        }
    }
		
	public float AnimSpeed()
	{
		return speedF / 12;
	}

    public void ChangeSpeed(float d)
    {
        speedS = 0;
        speedF = d;
    }

    public void ChangeSpeedMultiplier(float d)
    {
        if (d > 0)
            speedMult = d;
        else speedMult = 1.0f; 
    }

    public float getSpeed()
    {
        return speedF;
    }

    public void SetDifficulty(int d)
    {
        source.Play();
        difficulty = d;
        gravity = constGravity * difficulty;
        switch (d)
        {
            case 1:
                difficulty = 1.0f;
                break;
            case 2:
                difficulty = 1.3f;
                break;
            case 3:
                difficulty = 1.6f;
                break;
            case 4:
                difficulty = 2.0f;
                break;
            default:
                difficulty = 1.0f;
                break;
        }
        speedF = constSpeed * difficulty;
        ChangeSpeed(0.0f);
        camera.GetComponent<CameraMotor>().ResetCamera();
    }
}
