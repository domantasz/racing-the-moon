﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using Assets.Scripts.Achievements;
using Assets.Scripts.Skills;

namespace Assets.Scripts
{
    [Serializable]

    public class Data
    {
        public const int maxSize = 10;
        private int totalScore;
        private int[] highscores;
        private AchievementsContainer achievements;
        private int deathcount;
        private int damageTakenTotal;
        private int maxDamageTakenInSingleRun;
        private int maxScoreCombo;
        private long stayedAliveInVeteranTime;
        private long inGameTime;
        private long maxDidNotTakeDamageTime;
        private int safeModeCollected;
        private int speedUpCollected;
        private int speedDownCollected;
        private int doubleScoreCollected;
        public bool BuffSkillUnlocked { get; private set; }
        public bool BerserkSkillUnlocked { get; private set; }
        public bool ScreamSkillUnlocked { get; private set; }
        public bool SecondLifeSkillUnlocked { get; private set; }
        public bool BreakGroundSkillUnlocked { get; private set; }
        public int BuffSkillPrice { get; private set; }
        public int BerserkSkillPrice { get; private set; }
        public int ScreamSkillPrice { get; private set; }
        public int SecondLifeSkillPrice { get; private set; }
        public int BreakGroundSkillPrice { get; private set; }

        public Data()
        {
            totalScore = 0;
            highscores = new int[maxSize];
            achievements = new AchievementsContainer();
            deathcount = 0;
            damageTakenTotal = 0;
            maxDamageTakenInSingleRun = 0;
            maxScoreCombo = 0;
            stayedAliveInVeteranTime = 0;
            inGameTime = 0;
            maxDidNotTakeDamageTime = 0;
            safeModeCollected = 0;
            speedDownCollected = 0;
            speedUpCollected = 0;
            doubleScoreCollected = 0;
            BuffSkillUnlocked = false;
            BerserkSkillUnlocked = false;
            ScreamSkillUnlocked = false;
            SecondLifeSkillUnlocked = false;
            BreakGroundSkillUnlocked = false;
            BreakGroundSkillPrice = 1000000;
            SecondLifeSkillPrice = 5000000;
            ScreamSkillPrice = 500000;
            BerserkSkillPrice = 50000;
            BuffSkillPrice = 10000;
        }

        public bool BuyBuffSkill()
        {
            if(totalScore >= BuffSkillPrice)
            {
                totalScore -= BuffSkillPrice;
                BuffSkillUnlocked = true;
            }
            return BuffSkillUnlocked;
        }

        public bool BuyBerserkSkill()
        {
            if (totalScore >= BerserkSkillPrice)
            {
                totalScore -= BerserkSkillPrice;
                BerserkSkillUnlocked = true;
            }
            return BerserkSkillUnlocked;
        }

        public bool BuyScreamSkill()
        {
            if (totalScore >= ScreamSkillPrice)
            {
                totalScore -= ScreamSkillPrice;
                ScreamSkillUnlocked = true;
            }
            return ScreamSkillUnlocked;
        }

        public bool BuyBreakGroundSkill()
        {
            if (totalScore >= BreakGroundSkillPrice)
            {
                totalScore -= BreakGroundSkillPrice;
                BreakGroundSkillUnlocked = true;
            }
            return BreakGroundSkillUnlocked;
        }

        public bool BuySecondLifeSkill()
        {
            if (totalScore >= SecondLifeSkillPrice)
            {
                totalScore -= SecondLifeSkillPrice;
                SecondLifeSkillUnlocked = true;
            }
            return SecondLifeSkillUnlocked;
        }

        public List<Skill> GetSkills()
        {
            List<Skill> skills = new List<Skill>();
            string BuffDesc = "Buff activation becomes manual. Player can hold only 1 buff. Picking other buffs will override current one. Skill activates with Numpad4";
            string BersDesc = "When skill is activated, for 10 seconds enemy will do 4x less damage and will give extra points. Skill activates with Numpad2";
            string ScreamDesc = "When skill is activated, enemy will disappear within players field of view. Cooldown: 10 seconds. Skill activates with Numpad5";
            string BreakDesc = "When skill is activated, a tunnel will apprear under the player. Cannot be activated on bridge and inside the tunnel. Cooldown: 30 seconds. Skill activates with Numpad3";
            string LifeDesc = "Player permanently gains second life";


            skills.Add(new Skill("Buff", BuffDesc, BuffSkillPrice, BuffSkillUnlocked, 0));
            skills.Add(new Skill("Berserk", BersDesc, BerserkSkillPrice, BerserkSkillUnlocked, 1));
            skills.Add(new Skill("Howl", ScreamDesc, ScreamSkillPrice, ScreamSkillUnlocked, 2));
            skills.Add(new Skill("Break Ground", BreakDesc, BreakGroundSkillPrice, BreakGroundSkillUnlocked, 3));
            skills.Add(new Skill("Second Life", LifeDesc, SecondLifeSkillPrice, SecondLifeSkillUnlocked, 4));

            return skills;
        }

        public Achievement AddDeath()
        {
            Achievement unlocked = null;
            deathcount++;
            while (achievements.deathCountOver.TryToUnlock(deathcount.ToString()))
                unlocked = achievements.deathCountOver;
            return unlocked;
        }

        public Achievement AddInGameTime(long time)
        {
            Achievement unlocked = null;
            inGameTime += time;
            while (achievements.inGameTime.TryToUnlock(inGameTime.ToString()))
                unlocked = achievements.inGameTime;
            return unlocked;
        }

        public Achievement AddDamage(int dmg)
        {
            Achievement unlocked = null;
            damageTakenTotal += dmg;
            while (achievements.damageTakenTotal.TryToUnlock(damageTakenTotal.ToString())) ;
            return unlocked;
        }

        public Achievement maxDamage(int dmg)
        {
            Achievement unlocked = null;
            if (dmg > maxDamageTakenInSingleRun)
            {
                maxDamageTakenInSingleRun = dmg;
                while (achievements.damageTakenInSingleRun.TryToUnlock(maxDamageTakenInSingleRun.ToString()))
                    unlocked = achievements.damageTakenInSingleRun;
            }
            return unlocked;
        }

        public Achievement AddScore(int score)
        {
            Achievement unlocked = null;
            if (highscores[maxSize - 1] < score)
                highscores[maxSize - 1] = score;
            totalScore += score;
            Array.Sort<int>(highscores,
                        new Comparison<int>((i2, i1) => i1.CompareTo(i2)));
            while (achievements.totalScoreOver.TryToUnlock(totalScore.ToString()))
                unlocked = achievements.totalScoreOver;
            return unlocked;

        }

        public Achievement highScore()
        {
            Achievement unlocked = null;
            while (achievements.singleRunScoreOver.TryToUnlock(highscores[0].ToString()))
                unlocked = achievements.singleRunScoreOver;
            return unlocked;
        }

        public int GetTotalScore()
        {
            return totalScore;
        }

        public int[] GetHighscores()
        {

            return highscores;
        }

        public SimpleAchievement CheckHighscores()
        {
            SimpleAchievement unlocked = null;
            if (achievements.checkHighscores.TryToUnlock(true.ToString()))
                unlocked = achievements.checkHighscores;
            return unlocked;
        }
        public Achievement ScoreCombo(int combo)
        {
            Achievement unlocked = null;
            if (combo > maxScoreCombo)
            {
                maxScoreCombo = combo;
                while (achievements.scoreCombo.TryToUnlock(maxScoreCombo.ToString()))
                    unlocked = achievements.scoreCombo;
            }
            return unlocked;
        }

        public Achievement StayAliveInVeteranTime(long time)
        {
            Achievement unlocked = null;
            if (stayedAliveInVeteranTime < time)
            {
                stayedAliveInVeteranTime = time;
                while (achievements.stayAliveInVeteran.TryToUnlock(stayedAliveInVeteranTime.ToString()))
                    unlocked = achievements.stayAliveInVeteran;
            }
            return unlocked;
        }

        public Achievement DidNotTakeDmg(long time)
        {
            Achievement unlocked = null;
            if (time > maxDidNotTakeDamageTime)
            {
                maxDidNotTakeDamageTime = time;
                while (achievements.doNotTakeDamage.TryToUnlock(maxDidNotTakeDamageTime.ToString()))
                    unlocked = achievements.doNotTakeDamage;
            }
            return unlocked;
        }

        public Achievement addSafe()
        {
            Achievement unlocked = null;
            safeModeCollected++;
            while (achievements.safeModeCollected.TryToUnlock(safeModeCollected.ToString()))
                unlocked = achievements.safeModeCollected;
            return unlocked;
        }

        public Achievement addSpeedUp()
        {
            Achievement unlocked = null;
            speedUpCollected++;
            while (achievements.speedUpCollected.TryToUnlock(speedUpCollected.ToString()))
                unlocked = achievements.speedUpCollected;
            return unlocked;
        }

        public Achievement addSpeedDown()
        {
            Achievement unlocked = null;
            speedDownCollected++;
            while (achievements.speedDownCollected.TryToUnlock(speedDownCollected.ToString()))
                unlocked = null;
            return unlocked;
        }

        public Achievement addDoubleScore()
        {
            Achievement unlocked = null;
            doubleScoreCollected++;
            while (achievements.doubleScoreCollected.TryToUnlock(doubleScoreCollected.ToString()))
                unlocked = achievements.doubleScoreCollected;
            return unlocked;
        }

        public List<Achievement> GetAchievements()
        {
            List<Achievement> achievements = new List<Achievement>();

            achievements.Add(this.achievements.damageTakenInSingleRun);
            achievements.Add(this.achievements.damageTakenTotal);
            achievements.Add(this.achievements.deathCountOver);
            achievements.Add(this.achievements.doNotTakeDamage);
            achievements.Add(this.achievements.doubleScoreCollected);
            achievements.Add(this.achievements.inGameTime);
            achievements.Add(this.achievements.safeModeCollected);
            achievements.Add(this.achievements.scoreCombo);
            achievements.Add(this.achievements.singleRunScoreOver);
            achievements.Add(this.achievements.speedDownCollected);
            achievements.Add(this.achievements.speedUpCollected);
            achievements.Add(this.achievements.stayAliveInVeteran);
            achievements.Add(this.achievements.totalScoreOver);

            return achievements;
        }

        public List<SimpleAchievement> GetSimpleAchievements()
        {
            List<SimpleAchievement> achievements = new List<SimpleAchievement>();

            achievements.Add(this.achievements.checkHighscores);

            return achievements;
        }

        
    }
}
