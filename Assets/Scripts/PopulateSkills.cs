﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Assets.Scripts.Skills;

namespace Assets.Scripts
{
    class PopulateSkills : MonoBehaviour
    {
        public Transform Container;
        public Transform Item;
        public Transform Player;
        public Sprite[] sprites;

        public void Populate()
        {
            foreach (Transform child in Container)
                GameObject.Destroy(child.gameObject);
            List<Skill> skills = Player.GetComponent<StatController>().GetSkills();
            foreach (Skill skill in skills)
            {
                try
                {
                    Transform go = Instantiate(Item, Container);
                    Transform t = go.FindChild("Title");
                    t.GetComponent<Text>().text = skill.title;
                    t = go.FindChild("Description");
                    t.GetComponent<Text>().text = skill.description;
                    t = go.FindChild("Unlock");
                    if (skill.unlocked)
                    {
                        t.FindChild("Text").GetComponent<Text>().text = "Unlocked";
                        t.GetComponent<Button>().enabled = false;
                    }
                    else
                    {
                        t.FindChild("Text").GetComponent<Text>().text = "Buy for " + skill.price;
                        t.GetComponent<Button>().enabled = true;
                    }
                    t = go.Find("Image");
                    t.GetComponent<Image>().sprite = sprites[skill.spriteIndex];


                }
                catch (Exception e)
                {

                }
            }
        }
    }
}
