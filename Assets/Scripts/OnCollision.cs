﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnCollision : MonoBehaviour {
	public float Max_Health = 100f;
	public float Health = 0;
	public float DMG_Enemy = 20f;
	public float DMG_Obsticle = 10f;
    public float DMG_Wall = 5f;
    public float HP_Food = 10f;
    public bool safeMode = false;
    public Transform background;
    public Transform death;
    public Transform StartingPoint;
    private StatController stat;
    public Slider hp_bar;
	public Text hp_p;
    public BloodRainCameraController bloodRainController;
    public AudioSource source;
    public Animation wolf_anim;
    public AudioClip hit;
    public AudioClip food;
    public AudioClip water;
    public GameObject foodp;
    public GameObject waterp;
    public Transform camera;
    private ParticleSystem fp;
    private ParticleSystem wp;

    void Start()
	{
        fp = foodp.GetComponent<ParticleSystem>();
        wp = waterp.GetComponent<ParticleSystem>();
        stat = GetComponent<StatController>();
		Health = Max_Health;
		UpdateUI ();
	}

    void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "food") {

            source.PlayOneShot(food, 1);
            Health += HP_Food;
            fp.Play();
            other.gameObject.SetActive(false);
		}
        if (other.gameObject.tag == "water")
        {
            if (!wp.isPlaying && Health > 0)
            {
                GetComponent<PlayerMotion>().ChangeSpeed(0.0f);
                source.PlayOneShot(water, 1);
                wp.Play();
                Health = 0;
            }
        }
        if (other.gameObject.tag == "enemy") {
            if (!safeMode)
            {
                source.PlayOneShot(hit, 1);
                Health -= DMG_Enemy;
                stat.AddDmg((int)DMG_Enemy);
                if (StartingPoint.GetComponent<ActiveSkills>().BerserkActive)
                GetComponent<Score>().AddBonusScore(20);
                GetComponent<PlayerMotion>().ChangeSpeed(0.0f);
                try {
                    other.gameObject.GetComponent<Animation>().Play();
                }
                catch (Exception e) { }
				//Animations
				wolf_anim.Play("03_creep");
            }
            other.gameObject.SetActive(false);
        }
		if (other.gameObject.tag == "kliutis") {
            if (!safeMode)
            {
                source.PlayOneShot(hit, 1);
                Health -= DMG_Obsticle;
                stat.AddDmg((int)DMG_Obsticle);
                GetComponent<PlayerMotion>().ChangeSpeed(0.0f);
				//Animations
				wolf_anim.Play("03_creep");
            }
            other.gameObject.SetActive(false);
         }
        if (other.gameObject.tag.Equals("Wall"))
        {
            if (!safeMode)
            {
                source.PlayOneShot(hit, 1);
                Health -= DMG_Wall;
                stat.AddDmg((int)DMG_Wall);
				//Animations
				wolf_anim.Play("03_creep");
            }
            GetComponent<PlayerMotion>().ChangeSpeed(-5.0f);
        }
        CalculateHealth();
    }

    void CalculateHealth()
    {
        if (Health > Max_Health)
            Health = Max_Health;
        else if (Health <= 0)
        {
            Health = 0;
            Death();
        }
        UpdateUI();
    }

	public void UpdateUI()
	{
        bloodRainController.HP = (int)(HP_percent() * 100);
		hp_bar.value = HP_percent ();
		//hp_bar.value = HP_percent();// Healthbar update
		hp_p.text = HP_percent()*100+"%";
	}

	float HP_percent()
	{
		return Health / Max_Health;
	}

    public void Death()
    {

        if (StartingPoint.GetComponent<ActiveSkills>().SecondLifeActive)
        {
            GetComponent<PlayerMotion>().ChangeSpeed(0.0f);
            if (!wp.isPlaying)
            {
                camera.GetComponent<CameraMotor>().ResetCamera();
                StartingPoint.GetComponent<ActiveSkills>().SecondLife();
            }
        }
        else
        {
            GetComponent<PlayerMotion>().ChangeSpeed(0.0f);
            if (!wp.isPlaying)
            {
                death.gameObject.SetActive(true);
                background.gameObject.SetActive(true);
                StopGame();
            }
        }

    }

    public void StopGame()
    {
        GetComponent<Score>().Death();
        source.Stop();
        GetComponent<Score>().ResetScore();
        Time.timeScale = 0;
    }

}
