﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Assets.Scripts;

public static class LoadSaveManager
{
    private static string dataPath = Application.persistentDataPath + "/gameData.data";

    public static void SaveGame(Data data)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream stream = new FileStream(dataPath, FileMode.Create);

        bf.Serialize(stream, data);
        stream.Close();
    }

    public static Data LoadData()
    {
        
        if (File.Exists(dataPath))
        {
            try {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream stream = new FileStream(dataPath, FileMode.Open);
                Data data = bf.Deserialize(stream) as Data;
                stream.Close();
                return data;
            }
            catch(Exception e)
            {
                return new Data();
            }
        }
        else return new Data();
    }
}





