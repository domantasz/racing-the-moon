﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Notifications : MonoBehaviour
{

    public Transform box;
    public Text text;

    private List<string> messages;
    private float timer;
    System.DateTime moment;
    float seconds;
    // Use this for initialization
    void Start()
    {
        messages = new List<string>();
        box.position = new Vector3(Screen.width / 2, Screen.height - 50, 0);
        timer = -3;
    }

    // Update is called once per frame
    void Update()
    {
        moment = System.DateTime.Now;
        seconds = moment.Second;
        if (messages.Count < 1 && Check(seconds))
        {
            box.gameObject.SetActive(false);
            timer = -3;
        }
            
        if (messages.Count > 0 && Check(seconds))
        {
            text.text = messages[0];
            messages.RemoveAt(0);
            timer = seconds;
            box.gameObject.SetActive(true);
        }
    }

    public void AddMessage(string message)
    {
        messages.Add(message);
    }
    private bool Check(float seconds)
    {
        if (timer - seconds >= 57)
            return true;
        if (seconds - timer >= 3)
            return true;
        return false;
    }
    public void ResetSeconds()
    {
        seconds = 0;
    }
}
