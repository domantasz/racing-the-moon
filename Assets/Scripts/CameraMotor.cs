﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMotor : MonoBehaviour {

	private Transform lookAt;
    private Vector3 offset;
	// Use this for initialization
	void Start () {
		lookAt = GameObject.FindGameObjectWithTag ("Player").transform;
        ResetCamera();
	}

    // Update is called once per frame
    void Update()
    {
        if (offset.x != 0)
            offset.x = 0;
        if (offset.y != 1.5f)
            offset.y = 1.5f;
        if (offset.z != -3)
        {
            if (offset.z > -3)
                offset.z -= Time.deltaTime;
            else offset.z = -3;
        }
        if (transform.rotation.x != 30)
        {
            Quaternion target = Quaternion.Euler(0.0f, 0.0f, 0.0f);
            if (transform.rotation.x > 30)
                transform.rotation = Quaternion.Slerp(transform.rotation, target, -Time.deltaTime);
            else transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime);
        }
        transform.position = lookAt.position + offset;
    }

    public void ResetCamera()
    {
        Quaternion target = Quaternion.Euler(90.0f, 0.0f, 0.0f);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, 1);
        offset = new Vector3(0.0f, 0.0f, 0.0f);
    }
}
