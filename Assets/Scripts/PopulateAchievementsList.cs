﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Achievements;
using System;

public class PopulateAchievementsList : MonoBehaviour {

    public Transform Container;
    public Transform Item;
    public Transform Player;
    public Sprite[] sprites;
    // Use this for initialization
    void Start () {
    }

    public void Populate()
    {
        foreach (Transform child  in Container)
            GameObject.Destroy(child.gameObject);
        List<Achievement> achievements = Player.GetComponent<StatController>().GetAchievements();
        foreach (Achievement achievement in achievements)
        {
            try
            {
                Transform go = Instantiate(Item, Container);
                Transform t = go.FindChild("Title");
                t.GetComponent<Text>().text = achievement.Title;
                t = go.FindChild("Description");
                t.GetComponent<Text>().text = achievement.Message;
                t = go.FindChild("Level");
                t.GetComponent<Text>().text = "Level " + achievement.Level + " of " + achievement.MaxLevel;
                t = go.FindChild("Image");
                if (achievement.Level == 0)
                    t.GetComponent<Image>().sprite = sprites[0];
                else
                {
                    if (achievement.Level + 1 == achievement.MaxLevel)
                        t.GetComponent<Image>().sprite = sprites[achievement.SpriteIndex];
                    else
                        t.GetComponent<Image>().sprite = sprites[achievement.SpriteIndex - 1];
                }
            }
            catch (Exception e)
            {

            }
        }
        List<SimpleAchievement> achievementsSimple = Player.GetComponent<StatController>().GetSimpleAchievements();
        foreach (SimpleAchievement achievement in achievementsSimple)
        {
            try
            {
                Transform go = Instantiate(Item, Container);
                Transform t = go.FindChild("Title");
                Transform g = go.Find("Image");
                t.GetComponent<Text>().text = achievement.Title;
                t = go.FindChild("Description");
                t.GetComponent<Text>().text = achievement.Message;
                t = go.FindChild("Level");
                if (achievement.isUnlocked)
                {
                    t.GetComponent<Text>().text = "Unlocked";
                    g.GetComponent<Image>().sprite = sprites[achievement.SpriteIndex];
                }

                else
                {
                    t.GetComponent<Text>().text = "Locked";
                    g.GetComponent<Image>().sprite = sprites[0];
                }
            }
            catch (Exception e)
            {

            }
        }
    }

	// Update is called once per frame
	void Update () {
	}
}
